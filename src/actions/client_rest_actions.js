export function setClients(clients){
    
    return {
        type: "SET_CLIENTS",
        payload: clients
    }
}

export function setClient(client){
  return {
    type: "SET_CLIENT",
    payload: client
  }
}




      
  export function newClient(client){
    
    return {
        type: "NEW_CLIENT",
        payload: client
    }
}

export function updateClient(client){
    
    return {
        type: "UPDATE_CLIENT",
        payload: client
    }
}



export  function fetchClients(){
    return dispatch => {
        
            fetch('/api/clients')
            .then(res => {
                if(res.ok) 
                    return res.json()
                else
                    return res
            }, err => console.log(err))
            .then(data => {if (data) dispatch(setClients(data))})
            .catch( reason => console.log("error fetching clients"))    
            
    }
}

export function fetchClient(id){
  return dispatch => {
    dispatch(setClient(null))    
    fetch('/api/clients/' + id)
    .then(res =>{
      if(res.ok)
        return res.json()
      else
        return res
    }, err => console.log(err))
    .then(data => { if (data) dispatch(setClient(data))})
    .catch( reason => console.log(reason))
    } 

  }









function handleResponse(response){
    
    if(response.ok){
        
        return response.json();
    }else{
        let error = new Error(response.statusText)
        error.response = response;
        console.log("throwing error")
        console.log(error)
        throw error;
    }
}




export function putClient(client){
    
    return dispatch => {
        
        return fetch('/api/clients/'+ client.cliente._id ,{
            method: 'put',
            body: JSON.stringify(client),
            headers: {
                "Content-type":"application/json"
            }
            
        }).then(handleResponse)
        .then(client => {
        
            return dispatch(updateClient(client.client))
            
        })
    }
}

export function postNewClient(data){
    
    return dispatch => {
        
        return fetch('/api/clients/',{
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-type":"application/json"
            }
            
        }).then(handleResponse)
        .then(data => dispatch(newClient(data.client)))
    }
    
}    
   

export function deleteClientRest(client_id){
    
    return dispatch => {
        
        return fetch('/api/clients/'+ client_id ,{
            method: 'delete',
            headers: {
                "Content-type":"application/json"
            }
            
        }).then(handleResponse)
        .then(client => {
        
            return dispatch(deleteClient(client_id))
            
        })
    }
}



export function deleteClient(client_id){
    
    return {
        type: "DELETE_CLIENT",
        payload: client_id
    }
}


export function toggleClient(client_id){
    return{
        type: "TOGGLE_CLIENT",
        payload: client_id
    }
}

