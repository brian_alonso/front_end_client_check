export function showSlidePanel(){
    
    return {
        type: "SHOW_SLIDE_PANEL"
        
    }
}

export function hideSlidePanel(client){
    return {
        type: "HIDE_SLIDE_PANEL"
        
    }
}
