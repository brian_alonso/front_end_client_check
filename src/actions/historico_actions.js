export function selectRange(range){
    return {
        type: "SET_RANGE",
        payload: range
        
    }
}

export function setMax(max){
    return {
      type: "SET_MAX",
      payload: max
    }
}

export function setMin(min){
  return{
    type: "SET_MIN",
    payload: min
  }
}
