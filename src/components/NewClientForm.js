import React, { Component } from 'react';
import classnames from 'classnames';


class NewClientForm extends Component {
    
 
  componentDidMount(){
    this.props.onLoad()
  }
  
  render(){
   
   
    
    return (
    <div>
      <div className="field ">
        <label className="label is-danger">Servicio</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.serv})} 
                    name="serv"
                    defaultValue={this.props.client && this.props.client.get('serv')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Numero de SERV" />
          </div>
          <p className="help is-danger">
            {this.props.errors.serv}
          </p>
      </div>
      <div className="field">
        <label className="label">Cliente</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.cliente})} 
                    name="cliente"
                    defaultValue={this.props.client && this.props.client.get('cliente')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Nombre del cliente" />
          </div>
          <p className="help is-danger">
            {this.props.errors.cliente}
          </p>
      </div>
      <div className="field">
        <label className="label">Email</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.cliente})} 
                    name="email"
                    defaultValue={this.props.client && this.props.client.get('email')}
                    onChange={this.props.onChange} 
                    type="email" 
                    placeholder="Email del cliente" />
          </div>
          <p className="help is-danger">
            {this.props.errors.cliente}
          </p>
      </div>
      <div className="field">
        <label className="label">Equipo</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.equipo})} 
                    name="equipo"  
                    defaultValue={this.props.client && this.props.client.get('equipo')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Equipo IP al cual se conecta" />
          </div>
          <p className="help is-danger">
            {this.props.errors.equipo}
          </p>
      </div>
      <div className="field">
        <label className="label">Sitio</label>
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.sitio})} 
                    name="sitio"  
                    defaultValue={this.props.client && this.props.client.get('sitio')}
                    onChange={this.props.onChange} 
                    type="text" 
                    placeholder="Nombre del sitio" />
          </div>
          <p className="help is-danger">
            {this.props.errors.sitio}
          </p>
      </div>
      
      
        <label className="label">Dirección IP</label>      
      <div className="field is-grouped">
        
          <div className="control">
            <input  className={classnames('input',{'is-danger': !!this.props.errors.ip})} 
                    name="ip"  
                    onChange={this.props.onChange} 
                    defaultValue={this.props.client && this.props.client.get('ip')}
                    type="text" 
                    placeholder="" />
          </div>
          
         
          
      </div>
      <p className="help is-danger">
            {this.props.errors.ip}
      </p>
      
      
      <div className="field">
        <label className="label">Estado</label>
        <div className="control">
          <div className="select is-info ">
            <select 
              name="estado" 
              defaultValue={this.props.client && this.props.client.get('estado')}
              onChange={this.props.onChange}
              >
              <option value = "Activo" >Activo</option>
              <option value = "Desactivado">Desactivado</option>
              <option value = "Espera">Espera</option>
            </select>
          </div>
        </div>
      </div>
          
              
    </div>
    
    )
     
     
      
  }
}

export default NewClientForm
