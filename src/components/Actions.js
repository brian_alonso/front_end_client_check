import NewClientButton from '../containers/NewClientButton'
import NotificationsButton from '../containers/NotificationsButton'
import React, {Component} from 'react'




class Actions extends Component {

 
  
  
  render() {
    return (
      <div className="fb-item" id="actions">
                
                <ul>
                  <NewClientButton />
                  <NotificationsButton />
                </ul>
                
       </div>
    );
  }
}

export default Actions
