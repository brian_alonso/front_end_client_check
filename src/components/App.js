import React, { Component } from 'react';
import './App.css';
import Navbar from './Navbar'
import MainView from './Mainview'
import CreateClientModal from '../containers/CreateClientModal'
import DeleteClientModal from '../containers/DeleteClientModal'
import StateView from '../containers/StateView'
import ClientesCaidos from '../containers/ClientesCaidos'
import ClientView from '../containers/ClientView';
import SlidePanel from './SlidePanel'
// links for react-router

import {  Route, Switch } from 'react-router-dom'


class App extends Component {
    
  
  // render(){
  //   return(
  //         <div>
  //           <ClientList />
  //         </div>
  //         )
  // }
  
  render() {
    return (
      <div className="fb-container-column" >
        <SlidePanel />
        <Navbar />
        
        <Switch>
          <Route exact path="/clientes_caidos" component={ClientesCaidos} />
          <Route exact path='/clients' component={MainView}/>
          <Route exact path='/' component={StateView}/>
          <Route path="/clients/:id" component={ClientView}/>
        </Switch>
        <CreateClientModal />
        <DeleteClientModal />
      </div>
      
    );
  }

  

  
  
  
  
}




export default App;
