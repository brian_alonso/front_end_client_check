import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import StateOfInterface from '../containers/StateOfInterface.js'
class Navbar extends Component {
  render(){
    return (
     <nav className="navbar is-dark">
         <div className="navbar-brand">
            <div className="navbar-item">
                <Link to="/" >
                    <h1 className="big" >
                        ClientCheck 
                    </h1>
                </Link>
            </div>
         </div>
        <div className="navbar-item">
          <StateOfInterface />
        </div>
        <div className="navbar-menu">
            <div className="navbar-end">
            
                 <div className="navbar-item" >
                    <Link to="/" >
                     <button className="button is-danger ">
                        Ver Estado de Clientes
                     </button>
                    </Link>
                 </div>
                 
                 
                 <div className="navbar-item" >
                    <Link to="/clients" >
                     <button className="button is-info ">
                        Editar Clientes
                     </button>
                    </Link>
                 </div>
            
                
                 <div className="navbar-item" >
                    <Link to="/clientes_caidos" >
                     <button className="button is-warning ">
                        Clientes Caídos
                     </button>
                    </Link>
                 </div>
            
            
            
            </div>
           
                 
              
                 
           
        </div>
        
         
     </nav>
      );
  }
}

export default Navbar
