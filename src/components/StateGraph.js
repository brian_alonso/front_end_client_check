import React, { Component } from 'react'
import { Line } from 'react-chartjs-2' 
import moment from 'moment'


class StateGraph extends Component{
/*
  StateGraph resuelve el historico en formato crudo, lo adapta y genera la data.
  Además recibe el min y max para los límites de la gráfrica en los props

 <StateGraph historico={} > 

*/



  adaptar(historico, situacion){
    if (!historico) return ;
    let adaptado =historico.map( (el) => {
        let y = el.situacion === situacion ? 1 : 0
        return { t: moment(el.ultimo_cambio), y: y }
    })
    if( adaptado.slice(-1)[0].y === 1 ){
      adaptado.push({t: moment(), y: 1})
    }
    return adaptado
  }
 

  data(){
    return {
     datasets: [
        {       
          label: "Up",
          steppedLine: true,          
          data: this.adaptar(this.props.historicos, "Up"), 
          fill: true,
          backgroundColor: "#c6ffb3",
        },
        {       
          label: "Down",
          steppedLine: true,          
          data: this.adaptar(this.props.historicos, "Down"),
          fill: true,
          backgroundColor: "#ff0000",
          pointRadius: 0,
          pointHitRadius: 0,
          pointHoverRadius: 0

        },
        {       
          label: "Sin monitoreo",
          steppedLine: true,          
          data: this.adaptar(this.props.historicos, "Desactivado"),
          fill: true,
          backgroundColor: "#b3d9ff",
          pointRadius: 0,
          pointHitRadius: 0,
          pointHoverRadius: 0
        },
      
      ]
      }
    }
  
 
 
  render(){

    var chartData = this.data()
    var options = {
      maintainAspectRatio: true,
      responsive: true,
      scales: {
        yAxes: [
          {
            ticks: {
              maxTicksLimit: 1
            }

          }
        ],
        xAxes: [
          {
            type: 'time',
            distribution: 'linear',
            time:{
              max: this.props.max,
              min: this.props.min,
              displayFormats:{
              }
            }
          }
        ]
        
      },
      tooltips:{
        callbacks:{
          title: function(tooltipItem, chart){
            return tooltipItem[0].xLabel.calendar()
          }
        }
      }
            }
    
    return(
      <div>
        <div className="content">
          <h4>Linea de tiempo</h4>
          {this.props.children}
        </div>
        < Line data={chartData} options={options} width={900} height={100}/> 
      </div>
    );
  }
}


export default StateGraph
