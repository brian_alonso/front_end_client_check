import React, { Component } from 'react';
import classnames from 'classnames';
import moment from 'moment'
import { Link } from 'react-router-dom';
let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)



class StateHeader extends Component {
    

    
          
  
  
  
  
  render(){
    var articleClass = "is-success"
    var icon = "fa-check"
    switch(this.props.estado){
        case "Up":
            articleClass = "is-success"
            icon = "fa-check"
            break
        case "Down":
            articleClass = "is-danger"
            icon = "fa-bell"
            break
            
        case "Desactivado":
            articleClass = "is-dark"
            icon = "fa-pause"
            break
    }
    
    
    return (
        
    <section className={classnames("hero",articleClass)}>
        <div className="hero-body">
            <div className="container">
                <div className="columns is-vcentered">
                    <div className="column is-one-third is left"> 
                        <h1 className="title">
                            {this.props.nombreCliente}
                        </h1>
                        <h2 className="subtitle">
                            SERV-{this.props.serv}
                        </h2>      
                    
                    </div>
                    <div className="column">
                        <ul>
                            <li>AG: <strong>{this.props.ag}</strong></li>
                            <li>Sitio: <strong>{this.props.sitio}</strong></li>
                            <li>Ip: <strong>{this.props.ip}</strong></li>
                            
                        </ul>        
                    </div>
                </div>
              
          
            </div>
            </div>
        </section>
     
      );
  }
}

export default StateHeader



        /*
        <div>
            <article className={classnames('message is-small',articleClass)}>
              <div className="message-header">
                <p>
                    <Link to={"/clients/" + this.props._id} >
                        SERV-{this.props.serv}
                    </Link>
                </p>
                    <span className="icon is-small">
                        <i className={classnames('fa', icon)}></i>
                    </span>
         
              </div>
              <div className="message-body">
                <ul>
                    <li><strong>{this.props.nombreCliente}</strong></li>
                    <li>{this.props.ag}</li>
                    <li>{this.props.sitio}</li>
                    <li>{this.props.ip}</li>
                    <li>{moment(this.props.date).calendar()}</li>
                    
                </ul>
              </div>
            </article>
        </div> */