import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment'
import DatePicker from 'react-datepicker'
import classnames from 'classnames'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import 'react-datepicker/dist/react-datepicker.css'
import { hideSlidePanel } from '../actions/slide_panel_actions.js'
import { InputWithValidation } from '../_components/_formComponents'
import { validateNotification } from '../_validations'

/* para que este panel funcione se deben agregar las siguientes clases:
.left-panel {
  width: 300px;
  height: 400px;
  
  background-color: white;
  position: fixed;
  top: 150px;
  right: -275px;
  border-radius: 0 1em 1em 0;
  padding: 5px;
  z-index: 289;
  -webkit-transition: all 0.5s ease-in-out;
}

.left-panel:hover {
  right: 0px;
}

.left-panel.show {
  right: 0px;
}

*/



class SlidePanel extends Component {
    
    constructor(props){
        super(props)
        this.state ={
            type: "Tipo de Notificación",
            inicio: undefined,
            fin: undefined,
            ss: "",
            addText: "",
            tec: "",
            errors: {}
        }
    }
    
    _onChangeSelect = (event) => this.setState({type: event.target.value})

    _onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value})
    }

    

    _onSubmit = (e) => {
    const {errors, isValid} = validateNotification(this.state)
    this.setState({errors: errors})
    
    // const {usuario, nombre, apellido, inmobiliaria, email, password, confirmPassword} = this.state
    // var params = {
    //   name: nombre,
    //   apellido: apellido,
    //   username: inmobiliaria,
    //   email: email,
    //   password: password,
    //   confirmPassword: confirmPassword,
    //   usuario: usuario
    //         }
    // console.log("Calling action creator")
    // this.props.registerInmobiliaria(params, this.props.history)
  }


    
    
    setInicio = (date) => this.setState({inicio: date})
    setFin = (date) => this.setState({fin: date})
    
  render(){
       const type = this.state.type
       let tecTer = "TEC"
       let disableIncidente = {}
       let disableTarea = {}
       let disableDP2 = {}
       let disableSS = {}
      
       disableSS['disabled'] = true
       disableTarea['disabled'] = true
       disableIncidente['disabled'] = true
       disableDP2['disabled'] = true 
       
       if (type !== "Tipo de Notificación") {
        disableSS = {}
        if(type ==="Incidente"){
            disableIncidente = {}
            tecTer = "TER"
        }else{
            disableTarea = {}
            if(this.state.inicio) disableDP2 = {}
            tecTer = "TEC"
        }
       }else{
        disableSS['disabled'] = true
        disableTarea['disabled'] = true
        disableIncidente['disabled'] = true
       }
      
    return (
    <div className={classnames('panel left-panel',{'show-panel': this.props.slidePanel.get('show')})} >
        <p className="panel-heading">
            Notificar clientes 
            <span onClick={this.props.hideSlidePanel}className="icon is-small is-pulled-right" style={{marginTop: "1%", opacity: "0.5"}} >
                <i className="fa fa-times"></i>
            </span>
        </p>
        <div className="panel-block">
            <div className="field is-small">
                <p className="control has-icons-left">
                    <span className="select is-small">
                        <select onChange={this._onChangeSelect} value={type} className="is-small">
                            <option disabled>Tipo de Notificación</option>
                            <option>Incidente</option>
                            <option>Tarea Programada</option>
                            <option>Tarea de Emergencia</option>
                         </select>
                    </span>
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope"></i>
                    </span>
                </p>
        </div>
  
  
  </div>
  <div className="panel-block">
    <InputWithValidation
        errors = { this.state.errors.ss }
        value = { this.state.ss  }
        label = "SS Asociada"
        name = "ss"
        _onChange = {this._onChange}
        {...disableSS}
    />
  </div>
  <div className="panel-block">
    <InputWithValidation
        errors = { this.state.errors.tec }
        value = { this.state.tec  }
        label = {tecTer + "(hs)"}
        name = "tec"
        _onChange = {this._onChange}
        {...disableSS}
    />
  </div>
  
    <div className="panel-block">
        <div className="field">
            <label className="label is-small">Notas Adicionales<br/>(opcional)</label>
            <div className="control">
                <textarea 
                    className="textarea my-text-area" 
                    placeholder=""
                    {...disableSS}
                    onChange = {this._onChange}
                    name="addText"
                    value={this.state.addText}
                ></textarea>
            </div>
        </div>
    </div>
 
  
  
  <div className="panel-block">
    <div className="field">
        <label className="label is-small">Inicio/Fin</label>
        <div className="control">
             <DatePicker
                  {...disableTarea}
                  showTimeSelect
                  onChange={this.handleChange} 
                  timeFormat="HH:mm"
                  dateFormat="DD/MM/YY HH:mm" 
                  minDate={ moment() }
                  onChange={(e)=> this.setInicio(e)}
                  selected={ this.state.inicio  }
                  timeIntervals={15}
                  
         
              />
              <DatePicker
                  {...disableDP2}
                  
                 selected={ this.state.fin  }
                 showTimeSelect
                 minDate={ this.state.inicio || moment() }
                 onChange={this.handleChange} 
                  timeFormat="HH:mm"
                  timeIntervals={15}
                  dateFormat="DD/MM/YY HH:mm"
                  onChange={(e)=> this.setFin(e)}
      
              />
            <p className="help is-danger">
                {this.state.errors.inicioFin}
             </p>
  
        </div>
    </div>
   
  </div>
  
  
  
  <div className="panel-block">
    <button 
        {...disableSS}
        className="button is-success is-fullwidth"
        onClick={this._onSubmit}
    >
      Enviar notificaciones
    </button>
  </div>
</div>
      );
  }
}


 function mapStateToProps(state){
   return {
     slidePanel: state.slidePanel
   }
 }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    hideSlidePanel: hideSlidePanel
  }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(SlidePanel)


