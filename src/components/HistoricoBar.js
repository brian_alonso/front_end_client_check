import React, { Component } from 'react'
import { Bar  } from 'react-chartjs-2' 
import moment from 'moment'
import _ from 'underscore'
import CalcHours from '../helpers/CalcHours'

class HistoricoBar extends Component{
/*
  HistoricoBar resuelve el historico en formato crudo, lo adapta y genera la data.
  Además recibe el min y max para los límites de la gráfrica en los props

 <HistoricoBar historico={} > 


*/



  data(){
    var histo = this.props.historicos
    let resumen = CalcHours(histo, moment(this.props.min), moment(this.props.max))
 
    return {
      
     labels: ["Total en horas para el intervalo dado"],
     datasets: [
        { 
          label: "Up" , 
          backgroundColor: "#c6ffb3",    
          data: [resumen.Up/3600000]
        },
        {
          label: "Down",
          backgroundColor: "#ff0000",
          data: [resumen.Down/3600000]

        }, 
       {
          label: "Sin monitoreo",
          backgroundColor: "#b3d9ff",
          data: [resumen.Desactivado/3600000]

        }
              ]
      }
    }
  
 
 
  render(){
    
    var chartData = this.data()
    var options = {
      maintainAspectRatio: true,
      responsive: true,
      scales: {

        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              min: 0
            }

          }
        ]
        
      },
      tooltips:{
        callbacks:{
          label: function(tooltipItem, chart){
            
            let hours = Math.floor(moment.duration(tooltipItem.yLabel, 'hours').asHours())
            let minutes = Math.floor(moment.duration(tooltipItem.yLabel, 'hours').asMinutes() - hours * 60)
            let horas = (hours === 1) ? 'hora' : 'horas'
            let minutos = (minutes === 1) ? 'minuto' : 'minutos'
            return hours + " " + horas +" " + minutes + " " + minutos
          }
        }
      }
            }
    
    return(
       <div>
        <div className="content">
          <h4>Total en horas</h4>
          
          {this.props.children}
        </div>
        < Bar data={chartData} options={options} width={900} height={100}/> 
      </div>
    );
  }
}


export default HistoricoBar
