import React, { Component } from 'react';
import classnames from 'classnames';
import moment from 'moment'
import { Link } from 'react-router-dom';
let esLocale = require('moment/locale/es')
moment.locale('es', esLocale)



class State extends Component {
    

    
          
  
  
  
  
  render(){
    var articleClass = "is-success"
    var icon = "fa-check"
    switch(this.props.estado){
        case "Up":
            articleClass = "is-success"
            icon = "fa-check"
            break
        case "Down":
            articleClass = "is-danger"
            icon = "fa-bell"
            break
            
        case "Desactivado":
            articleClass = "is-dark"
            icon = "fa-pause"
            break
    }
    
    
    return (
        <div className="column is-narrow is-1-fullhd is-2-widescreen is-3-desktop is-4-tablet is-6-mobile">
            <article className={classnames('message is-small',articleClass)}>
              <div className="message-header">
                <p>
                    <Link to={"/clients/" + this.props._id} >
                        SERV-{this.props.serv}
                    </Link>
                </p>
                    <span className="icon is-small">
                        <i className={classnames('fa', icon)}></i>
                    </span>
         
              </div>
              <div className="message-body">
                <ul>
                    <li><strong>{this.props.nombreCliente}</strong></li>
                    <li>{this.props.ag}</li>
                    <li>{this.props.sitio}</li>
                    <li>{this.props.ip}</li>
                    <li>{moment(this.props.date).calendar()}</li>
                    
                </ul>
              </div>
            </article>
        </div>
     
      );
  }
}

export default State
