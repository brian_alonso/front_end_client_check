import React,{Component} from 'react'

class Filtro extends Component{
  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange(e){
    this.props.onFilterChange(e.target.value)
  }
  
  render(){
    const filterString = this.props.filterString;
    return(
      <div id="filter">
        
        <input placeholder="Filtro" className="input is-info is-small" type="text" value={filterString} onChange={this.handleChange}/>
        
      </div>
      );
  }
}


export default Filtro