import React, { Component } from 'react'
import moment from 'moment'
import _ from 'underscore'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

class DatePickers extends Component{
/*
  DatePickers resuelve el historico en formato crudo, lo adapta y genera la data.
  Además recibe el min y max para los límites de la gráfrica en los props

 <DatePickers historico={} > 


*/
 componentDidMount(){
    this.props.setMin(moment(this.props.historicos[this.props.historicos.length-1].ultimo_cambio))
    this.props.setMax(moment())
 }


  render(){
    if(!this.props.min && !this.props.max) return null
    let show = (this.props.range!=="Ingresar intervalo")
    let maxTime = moment().hours(23).minutes(59)
    let minTime = moment().hours(0).minutes(0)
    if(this.props.min.isSame(this.props.max, 'day') ){
        maxTime = moment().hours(this.props.max.hours()).minutes(this.props.max.minutes())
        minTime = moment().hours(this.props.min.hours()).minutes(this.props.min.minutes())
    }
    if(this.props.min.isSame(this.props.max, 'day') ){
        if (this.props.min > this.props.max){
            this.props.setMin(moment().subtract(7,'days'))
            this.props.setMax(moment())
            alert("La fecha de Fin no puede ser menor a la fecha de inicio. Modifique los valores")
            
        }
    }
    return(
    <table >
        <thead><tr><th>Inicio</th><th>Fin</th></tr></thead>
        <tbody><tr><td>  
       <DatePicker
         minTime={moment().hours(0).minutes(0)}
         maxTime={maxTime}
         selected={ this.props.min  }
         showTimeSelect
         selectsStart
         disabled={show}
         startDate={ this.props.min}
         endDate={this.props.max}
         onChange={(e)=> this.props.setMin(e)}
         timeFormat="HH:mm"
         timeIntervals={15}
         dateFormat="DD/MM/YY HH:mm" 
         minDate={ moment(this.props.historicos[this.props.historicos.length-1].ultimo_cambio) }
         maxDate={ moment() }
       />
      </td>
      <td>  
        <DatePicker
         disabled={show}
         minTime={ minTime }
         maxTime={moment().hours(23).minutes(59)}
         selected={this.props.max}
         showTimeSelect
         startDate={ this.props.min}
         endDate={this.props.max}
         selectsEnd
         timeFormat="HH:mm"
         timeIntervals={15}
         dateFormat="DD/MM/YY HH:mm"
         minDate={ moment(this.props.historicos[this.props.historicos.length-1].ultimo_cambio) }
         maxDate= {moment() }
         onChange={(e)=> this.props.setMax(e)}
       />
      </td>
      </tr></tbody>
    </table>
    );
  }
}


export default DatePickers
