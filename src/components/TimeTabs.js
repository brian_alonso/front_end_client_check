import React, { Component} from 'react'
import classnames from 'classnames'

class TimeTabs extends Component{
  
  //debe recibir un array de Titulos para las pestañas
  //y debe recibir un handler para cuando le doy click
  //y debe recibir que label está activo
  
  componentWillMount(){
    this.props.select("Ingresar intervalo")
  }
  
  
  render(){
    
    var labels = this.props.labels
    var labelsTabs = labels.map( label => {
     return (
       <li key={label} className={classnames({'is-active': label===this.props.selected}) }>
         <a onClick={()=>this.props.select(label)}>
          <span>{label}</span>
         </a>
       </li>
     
     )
    })
    return( 
      <div className="tabs is-centered is-toggle ">
        <ul>
          {labelsTabs}        
        </ul>
      </div>
    );
  }
}

export default TimeTabs
