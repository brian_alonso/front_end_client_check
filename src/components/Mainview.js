import React, { Component } from 'react';
import Filtro from './Filtro'
import Table from '../containers/Table/Table'
import Actions from './Actions'



class MainView extends Component {
  constructor(props){
    super(props);
    this.state = {
      filterString: ""
    }
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }
  
  handleFilterChange(value){
    this.setState({filterString: value})
  }
  
  render(){
    return (
      
      <div className="fb-container-row" id="fb-body">
        <div className="fb-nav-left fb-item">
          <Actions />
          <Filtro onFilterChange={this.handleFilterChange} filterString={this.state.filterString}/>
          <div className="separador"></div>
          <Table filter={this.state.filterString}/>
        </div>
            
            
          
      </div>
      
      
      
      );
  }
}

export default MainView
