import React, { Component} from 'react'
import {showDeleteClientModal, setDeleteClientModalLoading, setDeleteClientId } from '../actions/delete_client_actions'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';



class DeleteClient extends Component {
     
  setModalParameters(){
      
      this.props.showDeleteClientModal()
      this.props.setDeleteClientId(this.props.client_id)
      
      
  }   
    
  
  render() {
     
    return (
             <span className="icon">
                  <i className="fa fa-remove" onClick={()=>this.setModalParameters()}></i>
              </span>
            
    );
  }
}



function matchDispatchToProps(dispatch){
  return bindActionCreators({
    showDeleteClientModal: showDeleteClientModal,
    setDeleteClientId: setDeleteClientId,
    setDeleteClientModalLoading: setDeleteClientModalLoading
    
    
  }, dispatch)
}

export default connect(null, matchDispatchToProps)(DeleteClient);
