import React, { Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {showEditModal, hideEditModal, setEditModalLoading} from '../actions/create_client_actions'
import {postNewClient, putClient } from '../actions/client_rest_actions'
import Modal from '../components/Modal'
import NewClientForm from '../components/NewClientForm'

class CreateClientModal extends Component {
  constructor(props){
    super(props)
    
    
      this.state = {
        serv:  '',
        cliente:  '',
        equipo:  '',
        sitio: '',
        ip:  '',
        estado:  'Activo',
        errors: {},
        _id: ''
     }
    this.onSave = this.onSave.bind(this);
    this.onLoad = this.onLoad.bind(this);
    this.onClose = this.onClose.bind(this);
    this.blankUser = this.blankUser.bind(this);
  }
  
   onLoad() {
     
     if(this.props.create_client_ui.get('edit_modal_client')){
       
       
       
       console.log("onLoad")
       let client = this.props.create_client_ui.get('edit_modal_client')
       this.setState({
                    
       _id: client.get('_id'),
       serv: client.get('serv'),
       cliente: client.get('cliente'),
       equipo: client.get('equipo'),
       sitio: client.get('sitio'),
       estado: client.get('estado'),
       ip: client.get('ip')
     })
    }
   }
  
   handleChange = (e) => {
        if(!!this.state.errors[e.target.name]){
            let errors = Object.assign({}, this.state.errors)
            delete errors[e.target.name]
            this.setState({
                [e.target.name]: e.target.value,
                errors
            })
        }else{
            this.setState({ [e.target.name]: e.target.value})    
        }
        
        
    }
  
  
   validateClient(){
     let errors = {};
        if(this.state.serv === '') errors.serv = "Este campo es obligatorio"
        if(!/^\d+$/.test(this.state.serv)) errors.serv = "Este campo solo acepta valores numericos. Ingrese el numero de SERV"
        if(this.state.cliente === '') errors.cliente = "Este campo es obligatorio"
        if(this.state.equipo === '') errors.equipo = "Este campo es obligatorio"
        if(this.state.sitio === '') errors.sitio = "Este campo es obligatorio"
        if(this.state.ip === '') errors.ip = "Este campo es obligatorio"
        if(!/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(this.state.ip)) errors.ip ="No es una ip válida"
        this.setState({errors})
        const isValid = Object.keys(errors).length === 0
        
        if(isValid) return true
        return false
   }
  
  
  
   onSave() {
     
    if(this.validateClient()){
        const action = this.props.create_client_ui.get('edit_modal_action')
        
        switch(action){
            case "POST":
                return this.saveNewClient()
                
            case "PUT":
                return this.editClient()
                
            default:
                return null
        }
    }
  }
  
  blankUser(){
   this.setState({
        serv:  '',
        cliente:  '',
        equipo:  '',
        sitio: '',
        ip:  '',
        email: '',
        estado:  'Activo',
        errors: {},
        _id: ''
     })
  }
  
  onClose() {
    this.props.hideEditModal()
    this.blankUser()
  }

  
  saveNewClient(){
        
    
            const {serv, cliente, equipo, sitio, ip, estado} = this.state
            
            this.props.setEditModalLoading(true)
            this.props.postNewClient({cliente: {serv,cliente,equipo,sitio,ip, estado}}).then(
                (data) => {
                            this.props.setEditModalLoading(false)
                            this.props.hideEditModal()
                            console.log(data)
                            this.blankUser()
                    
                },
                (err) => err.response.json().then(({errors})=> { 
                    this.props.setEditModalLoading(false)
                    this.props.hideEditModal()
                    this.blankUser()
                }
                )
            );
        
    }
    
  editClient(){
      
      const {serv, cliente, email, equipo, sitio, ip, estado, _id} = this.state
            
      this.props.setEditModalLoading(true)
      this.props.putClient({cliente: {serv,cliente, email, equipo,sitio,ip, estado, _id}}).then(
        (data) => {
                    this.props.setEditModalLoading(false)
                    this.props.hideEditModal()
                    this.blankUser()
                    
        },
        (err) => { console.log(err) 
                        
                    this.props.setEditModalLoading(false)
                    this.props.hideEditModal()
                    this.blankUser()
                        }
                    
        
            
      );
      
  }
  
  
  render(){
    
     
    return (
    <Modal 
      showModal={this.props.create_client_ui.get('showEditModalflag')}
      title={this.props.create_client_ui.get('edit_modal_title')}
      onClose={this.onClose}
      onConfirm={this.onSave}
      confirmButtonMessage="Guardar Cambios"
      confirmButtonClass="is-primary"
      cancelButtonClass="is-dark"
      cancelButtonMessage="Cancelar"
      loading={this.props.create_client_ui.get('edit_modal_loading')}
    >
    <NewClientForm errors={this.state.errors} client={this.props.create_client_ui.get('edit_modal_client')} onChange={this.handleChange} onLoad={this.onLoad}/>
    </Modal>
      )
     
     
      
  }
}

function mapStateToProps(state){
        return {
            create_client_ui: state.create_client_ui
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    showEditModal: showEditModal,
    hideEditModal: hideEditModal,
    postNewClient: postNewClient,
    setEditModalLoading: setEditModalLoading,
    putClient: putClient
  }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CreateClientModal);





