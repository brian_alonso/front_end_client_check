import React, { Component} from 'react'
import {showEditModal, setEditModalAction, setEditModalTitle, setEditClient} from '../actions/create_client_actions'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';



class EditClient extends Component {
     
  setModalParameters(){
      this.props.setEditModalAction("PUT")
      this.props.setEditModalTitle("Editar Cliente")
      this.props.showEditModal()
      this.props.setEditClient(this.props.client)
      console.log(this.props.client)
  }   
    
  
  render() {
     
    return (
             <span className="icon">
                  <i className="fa fa-pencil" onClick={()=>this.setModalParameters()}></i>
              </span>
            
    );
  }
}



function matchDispatchToProps(dispatch){
  return bindActionCreators({
    showEditModal: showEditModal,
    setEditModalAction: setEditModalAction,
    setEditModalTitle: setEditModalTitle,
    setEditClient: setEditClient
  }, dispatch)
}

export default connect(null, matchDispatchToProps)(EditClient);
