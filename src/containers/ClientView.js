import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Line } from 'react-chartjs-2' 
import moment from 'moment'
import { fetchClient } from '../actions/client_rest_actions.js'
import { selectRange, setMax, setMin } from '../actions/historico_actions.js'
import StateGraph from '../components/StateGraph';
import HistoricoTable from '../components/HistoricoTable';
import HistoricoBar from '../components/HistoricoBar';
import HistoricoBarResumen from '../components/HistoricoBarResumen';
import DatePickers from '../components/DatePickers';
import TimeTabs from '../components/TimeTabs';
import StateHeader from '../components/StateHeader'


class ClientView extends Component{
 constructor(props){
   super(props)
   this.calculateMin = this.calculateMin.bind(this);
   this.selectRange = this.selectRange.bind(this);
 }

  componentWillMount(){
    this.props.fetchClient(this.props.match.params.id)
  }
  
  

  calculateMin(label){
    let cant = 0
    let unidad = 'minutes'
    var myRegexp = /(\d+)(\w)/
    var match = myRegexp.exec(label)
    let traductor = { 
      D: "days",
      H: "hours",
      Y: "years",
      m: "minutes",
      M: "months",
      Y: "years"
    } 
    if(!match) return null  
    if(match[1]) cant = parseInt(match[1])
    if(match[2]) unidad = traductor[match[2]]
    return moment().subtract(cant, unidad) 
  }

  selectRange(label){
    this.props.setMax(moment())
    this.props.setMin(this.calculateMin(label) || moment(this.props.historico.get("client").historicos[0].ultimo_cambio))
    this.props.selectRange(label)
  } 


  render(){
    let range = this.props.historico.get('range')
    
    if(!this.props.historico.get('client') ) return null
    let client = this.props.historico.get('client')
    if(this.props.historico.get('client').historicos.length < 1) return (
      <div>
      <br />
       <StateHeader
            
            estado={client.situacion}
            nombreCliente={client.cliente}
            serv = {client.serv}
            ag = {client.equipo}
            sitio = {client.sitio}
            date = {client.ultimo_cambio}
            ip = {client.ip}
            _id = {client._id}
        />  
        Aun no hay datos históricos para mostrar
       <br />
       </div>
      )
   // let client = this.props.historico.get('client')
    let historicos = this.props.historico.get("client").historicos
    let historicosInverse = historicos.slice(0).sort((b,a) => a.ultimo_cambio.localeCompare(b.ultimo_cambio))
        return(
      <div>
      <br />
       <StateHeader
            
            estado={client.situacion}
            nombreCliente={client.cliente}
            serv = {client.serv}
            ag = {client.equipo}
            sitio = {client.sitio}
            date = {client.ultimo_cambio}
            ip = {client.ip}
            _id = {client._id}
        />  
       <br /> 
       <TimeTabs 
          selected={range} 
          select={ this.selectRange }
          labels={['Ingresar intervalo', '15m','1H','6H','12H','1D','3D','7D','15D','1M','3M','6M','1Y'] }
        />
       
       <section className="section">
         <StateGraph 
          historicos={historicos}
          range={range}
          min={this.props.historico.get("min")}
          max={this.props.historico.get("max")}
        >
          <DatePickers 
          historicos={historicosInverse}
          range={range}
          min={this.props.historico.get("min")}
          max={this.props.historico.get("max")}
          setMin={this.props.setMin}
          setMax={this.props.setMax} 
        />
        </StateGraph>
       </section>
       <section className="section">
         <HistoricoBar
            historicos={historicos}
          
            range={range}
            min={this.props.historico.get("min")}
            max={this.props.historico.get("max")}
         >
        <DatePickers 
          historicos={historicosInverse}
          range={range}
          min={this.props.historico.get("min")}
          max={this.props.historico.get("max")}
          setMin={this.props.setMin}
          setMax={this.props.setMax} 
        />
      </HistoricoBar>
       </section>
       <section className="section">
        <HistoricoBarResumen
            historicos={historicos}
            range={range}
            min={this.props.historico.get("min")}
            max={this.props.historico.get("max")}
         />
       </section>
       <section className="section">
               <HistoricoTable historicos={historicosInverse} titles={['Fecha','Estado','Tiempo']}/>
       </section>
       
       
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    historico: state.historico
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    fetchClient: fetchClient,
    selectRange: selectRange,
    setMax: setMax,
    setMin: setMin
  }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ClientView)
