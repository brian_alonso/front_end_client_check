import React, { Component} from 'react'

class StateOfClient extends Component{


  
  render(){
   let situacion = this.props.client.get("situacion") 

    switch(this.props.client.get("situacion")){
      case "Up": 
        situacion = <span className="icon has-text-success"><i className="fa fa-thumbs-up" aria-hidden="true"></i></span>
        break;
      case "Down":
        situacion = <span className="icon has-text-danger"><i className="fa fa-thumbs-down" aria-hidden="true"></i></span>
        break;
      case "Desactivado":
        situacion = <span className="icon has-text-info"><i className="fa fa-pause-circle-o" aria-hidden="true"></i></span>
        break;
      default:
        situacion = "Default"
            
    }
            
    console.log("situacion") 
    

    return( 
      <span>{situacion}</span>
   );
  }
}

export default StateOfClient
