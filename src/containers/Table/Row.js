import React, { Component} from 'react'
import {Map} from 'immutable';
import moment from 'moment'
import uuid from '../../helpers/generateUUID'
import EditClientButton from '../EditClientButton'
import DeleteClientButton from '../DeleteClientButton'
import StateOfClient from '../StateOfClient'


class Row extends Component {
  constructor(props){
    super(props);
    this.state = { edit: false }
    this.handleClickEdit = this.handleClickEdit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.modifiedClient = Map(this.props.client)
  
  };
  
  
  
  
  handleClickEdit() {
    this.setState({edit: !this.state.edit})
    this.props.tableProps.updateClient(this.modifiedClient)
      //set_state
  }
  
  handleChange(attr, e){
      this.modifiedClient = this.modifiedClient.set(attr, e.target.value)
  }
  
  
  
  render() {

    const client = this.props.client
    console.log(client)

    var attrs = ["cliente", "equipo" , "sitio" , "ip","email", "estado"]

    var printTd = (immut,attr) => <td key={uuid()}>{immut.get(attr)}</td>    


    var printFields = (immut,attrs, printFunc) => {
        return  attrs.map( (attr)=> {
          return printFunc(immut,attr);
    })
    
  }


    



var servStyle = {
      color: 'black',
      fontSize: "72%",
      paddingTop: "10%"
    };

   
    
    
    
    return(
    
        <tr>
              <td> <input onChange={(e)=>this.props.handleChBoxClick(client.get("_id")) } type="checkbox" checked={client.get("checked")}/></td>
              <td>SERV-{client.get("serv")}</td>
              {printFields(client ,attrs, printTd)}
              
              <td style={{fontSize: "70%"}}>
                
                {moment(client.get("ultimo_cambio")).calendar()}
                
              </td>
		<td>
		    <StateOfClient client={client} />
		</td>
        <td>
                <EditClientButton client={client} />
                <DeleteClientButton client_id={client.get("_id")} />
              </td>
            </tr>
    
        
    )
    
  }
}

export default Row