import React, { Component} from 'react'
import {showEditModal, setEditModalAction, setEditModalTitle, setEditClient} from '../actions/create_client_actions'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';



class NewClient extends Component {
     
  setModalParameters(){
      this.props.setEditModalAction("POST")
      this.props.setEditModalTitle("Nuevo Cliente")
      this.props.setEditClient(null)
      this.props.showEditModal()
  }   
    
  
  render() {
     
    return (
            
                <li className="button is-info" onClick={()=>this.setModalParameters()}>Agregar Cliente</li>
            
    );
  }
}



function matchDispatchToProps(dispatch){
  return bindActionCreators({
    showEditModal: showEditModal,
    setEditModalAction: setEditModalAction,
    setEditModalTitle: setEditModalTitle,
    setEditClient: setEditClient
  }, dispatch)
}

export default connect(null, matchDispatchToProps)(NewClient);
