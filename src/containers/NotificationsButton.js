import React, { Component} from 'react'
//import { notificationButtonAction } from '../actions/ClientsReducer'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {showSlidePanel} from '../actions/slide_panel_actions.js'


class NotificationsButton extends Component {
     
  
  render() {
     return (
            
                <li className="button is-danger" onClick={()=>this.props.notificationButtonAction()}>Notificar</li>
            
    );
     
  }
}

function dispararNotification(){
  return {
        type: "NOTIFICATION",
        payload: null
    }
}


function matchDispatchToProps(dispatch){
  return bindActionCreators({
    notificationButtonAction: showSlidePanel,
    
  }, dispatch)
}

export default connect(null, matchDispatchToProps)(NotificationsButton);
