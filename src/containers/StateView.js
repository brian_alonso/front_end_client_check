import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import {fetchClients} from '../actions/client_rest_actions.js'
import React, { Component } from 'react';
import State from '../components/State';
import Filtro from '../components/Filtro';
import moment from 'moment';




class StateView extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      filterString: ""
    }
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }
  
  handleFilterChange(value){
    this.setState({filterString: value})
  }
  
  
  
  componentWillMount(){
      this.props.fetchClients();
  }
  
  filterSort(clients, situacion) {
    let filtered = clients.filter(
          (el) => el.get("situacion") === situacion
    )
    
    let sorted = filtered.sort(
        (b, a) => a.get('ultimo_cambio').localeCompare(b.get('ultimo_cambio'))    
    )
    return sorted
  }
  
  renderClient(clients){
      let situaciones = clients.map((cliente, index) => 
        <State 
            key={index} 
            estado={cliente.get('situacion')}
            nombreCliente={cliente.get('cliente')}
            serv = {cliente.get('serv')}
            ag = {cliente.get('equipo')}
            sitio = {cliente.get('sitio')}
            date = {cliente.get('ultimo_cambio')}
            ip = {cliente.get('ip')}
            _id = {cliente.get('_id')}
        />  
    );
    return situaciones
  }
      
  
  render(){
    
    let clients = this.props.clients
    var filter = this.state.filterString;
    var filteredClients = clients.filter((cliente) =>{
      console.log("********")
      console.log(cliente)
      var passedfilter =  cliente.filter( (attr,key) => {
          let attrcopy = attr
          if (typeof attr === "number") attrcopy = attr + ''
          if (key === "alta") attrcopy = moment(attr).calendar()
          if (key === "historico") attrcopy = ""
          if (key !== "_id" && attrcopy && attrcopy.toLowerCase().includes(filter.toLowerCase())){
             return true
          }
	      return false
      });
      
      if (passedfilter.size > 0) return true
      
      
      
    //   for(var key in cliente){
    //     console.log(key + " " + cliente)
    //     if (cliente.get(key) && cliente.get(key).toLowerCase().includes(filter.toLowerCase())){
    //       return true
    //     }
    //   }
     return false
    });
    
    let down_sorted = this.filterSort(filteredClients,"Down"); 
    let up_sorted = this.filterSort(filteredClients, "Up");
    let desactivado_sorted = this.filterSort(filteredClients, "Desactivado");
    
    
    
    
    return (
        <div>
        
            <br />
            <Filtro onFilterChange={this.handleFilterChange} filterString={this.state.filterString}/>
            <br />
            
            <div className="columns is-multiline">
                {this.renderClient(down_sorted)}
                {this.renderClient(up_sorted)}
                {this.renderClient(desactivado_sorted)}
            </div>
        </div>
           
            
     
      );
  }
}


function mapStateToProps(state){
        return {
            clients: state.clients
        }
    }

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    fetchClients: fetchClients

  }, dispatch)
}



//export default ClientList

export default connect(mapStateToProps, matchDispatchToProps)(StateView) ; //now it is a smart component, container



