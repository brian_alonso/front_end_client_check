import React from 'react';
import ReactDOM from 'react-dom';
import 'bulma/css/bulma.css';
import 'font-awesome/css/font-awesome.min.css'
import thunk from 'redux-thunk'
import './index.css';
import App from './components/App';
//import registerServiceWorker from './registerServiceWorker';
import socket from './socket/socket.js'
import allReducers from './reducers' //webpack by default import index.js
                                    // in reducers/index.js there's not need to export

//router

import { BrowserRouter } from 'react-router-dom'


//redux provider

import {Provider} from 'react-redux'


//redux creating store and middleware

import {createStore, applyMiddleware} from 'redux';

//redux devtools extension

import { composeWithDevTools } from 'redux-devtools-extension'




const store = createStore(
  allReducers, 
  composeWithDevTools(
    applyMiddleware(thunk)
  )
);
  
  






ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App  />
    </BrowserRouter>
  </Provider>
    ,document.getElementById('root'));
//registerServiceWorker();


export default store;
