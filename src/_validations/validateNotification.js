// Funcion que valida la entrada del RegisterForm
import validator from 'validator'
//https://github.com/chriso/validator.js
import validateManyAttrs from './validateManyAttrs'


function validateNotification(data){
    let errors = {}
    
    
    //valida que los campos no estén vacíos
    validateManyAttrs(  
                        data, 
                        ["ss","tec"],
                        errors,
                        "Este campo es obligatorio",
                        validator.isEmpty
                     )
    
    if(!validator.isInt(data.ss)){
        errors.ss = "La ss debe ser un valor numérico"
    }
    
    if(!data.tec.match(/^\d{1,2}([:]\d{1,2}){0,1}$/)){
        errors.tec = "No es un formato de hora válido (hh:mm)"
    }
    
    if(data.type !== "Incidente"){
        if(!data.inicio || !data.fin){
            errors.inicioFin = "Debe ingresar inicio y fin de tarea."
        }else if(data.inicio.isAfter(data.fin)){
            
            errors.inicioFin = "La fecha de fin debe ser posterior a la de inicio."
        }
    }
        

    
    // if(!validator.equals(data.password, data.confirmPassword)){
    //     errors.confirmPassword = "La contraseña no coincide"
    // }
    
    // if(!validator.isLength(data.password,{min:6, max: undefined})){
    //     errors.password = "La constraseña debe tener al menos 6 caracteres"
    // }
    
    return {
        errors,
        isValid: Object.keys(errors).length === 0
    }
}

export { validateNotification } 

