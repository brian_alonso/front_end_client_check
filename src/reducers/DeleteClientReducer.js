import {Map} from 'immutable';

export default function(state= Map({
                        
                        showDeleteClientModalFlag: false,
                        
                        delete_modal_loading: false,
                        
                        delete_client_id: null
  
}), action) {
    
    
  
  
  switch(action.type){
    case "SHOW_DELETE_CLIENT_MODAL":
      
       return state.set('showDeleteClientModalFlag',  true );
    case "HIDE_DELETE_CLIENT_MODAL":
      
       return  state.set('showDeleteClientModalFlag',  false );  
    
    case "SET_DELETE_CLIENT_MODAL_LOADING":
        
        return state.set('delete_modal_loading', action.payload);
    case "SET_DELETE_CLIENT_ID":
      
        return state.set('delete_client_id', action.payload);
    
    default: return state;
  }  
      
   
}
