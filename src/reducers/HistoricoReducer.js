import Immutable, {List, Map} from 'immutable';


export default function(state=Map(
  {
    client: null,
    range: "Ingresar intervalo",
    max: null,
    min: null
  }
 ), action) {
    
    
  
  switch(action.type){
    case "SET_CLIENT":

       return state.set('client', action.payload);
    case "SET_RANGE":
        return state.set('range', action.payload); 
    case "SET_MAX":
        return state.set('max', action.payload);
    case "SET_MIN":
        return state.set('min', action.payload);
    default: return state;
  }  

  
      
}
