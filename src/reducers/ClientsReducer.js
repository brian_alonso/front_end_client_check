import Immutable, {List, Map} from 'immutable';


export default function(state=List(), action) {
    
    
  
  
  switch(action.type){
    case "NOTIFICATION":
      let algo = state.filter((client, index) => client.get("checked"))
      console.log(algo)
      return state
    case "SET_CLIENTS":
      
      // return List( [
      //               Map({a:"a"}), 
      //               Map({b:"b"})
                    
      //               ]) ;
      return Immutable.fromJS(action.payload);
    case "NEW_CLIENT":
      
      return state.push(Map(action.payload));
    
    case "UPDATE_CLIENT":
      
      return state.update(
      state.findIndex(function(item) { 
          
          return item.get("_id") === action.payload._id; 
        }), function(item) {
          action.payload.checked = item.get("checked")
          return Map(action.payload);
        }
      );
      
    case "TOGGLE_CLIENT":
      //immutable list, update( index, (itemToBeReplaced => newItem))
      return state.update( 
                            state.findIndex( (item)=> item.get("_id") === action.payload), 
                            (item)=> item.update('checked', value => !value)
      )
    
    // case "TOGGLE_CLIENT":
    //   return state.update(
    //     state.findIndex(function(item) { 
          
    //       return item.get("_id") === action.payload; 
    //     }), function(item) {
    //     // return item.update('checked', value => !item.get("checked"));
    //         return item.update('checked', value => !value)
          
    //     }
    //   );
      
      
    
    
    case "DELETE_CLIENT":
        return state.filterNot(
          (el) => el.get("_id") === action.payload
          )
    default: return state;
  }  
      
}
